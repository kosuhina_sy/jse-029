import org.junit.jupiter.api.Test;
import org.mockito.internal.matchers.apachecommons.ReflectionEquals;

import static org.junit.jupiter.api.Assertions.*;

class MathServiceTest {
    MathService mathService = new MathService();

    @Test
    void sum() {
        assertEquals(2, mathService.sum("1", "1"));
    }

    @Test
    void factorial() {
        assertEquals(5040, mathService.factorial("7") );
    }

    @Test
    void factorialZero() {
        assertEquals(0, mathService.factorial("0"));
    }

    @Test
    void factorialNegative() {
        assertThrows(IllegalArgumentException.class, () -> mathService.factorial("-1"));
    }

    @Test
    void factorialWrongChar() {
        assertThrows(IllegalArgumentException.class, () ->mathService.factorial("d"));
    }

    @Test
    void fibonacci() {
        long [] expected ={0, 1, 1, 2};
        long [] result = mathService.fibonacci("3");
        assertTrue(new ReflectionEquals(expected).matches(result));
    }

    @Test
    void fibonacciZero() {
        long[] expected = {0};
        long[] result = mathService.fibonacci("0");
        assertTrue(new ReflectionEquals(expected).matches(result));
    }
    @Test
    void fibonacciZeroOne() {
        long[] expected = {0, 1};
        long[] result = mathService.fibonacci("1");
        assertTrue(new ReflectionEquals(expected).matches(result));
    }
}