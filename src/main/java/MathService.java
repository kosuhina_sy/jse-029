public class MathService {
    public long sum(String arg1, String arg2) {
        return toLong(arg1) + toLong(arg2);
    }

    public long factorial(String arg) {
        long value = toLong(arg);
        isPositive(value);
        if (value == 0) {
            return 0;
        }
        long result = 1;
        for (int i = 1; i <= value; i++) {
            result *= i;
        }
        return result;
    }

    public long[] fibonacci(String arg) {
        int value = toInteger(arg);
        isPositive(value);
        long[] result = new long[value + 1];
        if (result.length == 1) {
            result[0] = 0;
            return result;
        }
        if (result.length == 2) {
            result[0] = 0;
            result[1] = 1;
            return result;
        }
        result[0] = 0;
        result[1] = 1;
        for (int i = 2; i < result.length; i++) {
            result[i] = result[i - 1] + result[i - 2];
        }
        return result;
    }


    private long toLong(String arg) {
        try {
            return Long.parseLong(arg);
        } catch (NumberFormatException | NullPointerException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
    }

    private int toInteger(String arg) {
        try {
            return Integer.parseInt(arg);
        } catch (NumberFormatException | NullPointerException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
    }

    private void isPositive (int arg) {
        if (arg < 0) {
            throw new IllegalArgumentException(arg + "<0");
        }
    }

    private void isPositive (long arg) {
        if (arg < 0) {
            throw new IllegalArgumentException(arg + "<0");
        }

    }

}
